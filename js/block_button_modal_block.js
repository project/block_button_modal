(function (Drupal) {
  Drupal.behaviors.block_button_modal_block = {
    attach(context, settings) {
      let wrappers = [];
      if (
        context.classList &&
        context.classList.contains('block-button-modal-block-wrapper')
      ) {
        wrappers.push(context);
      } else {
        wrappers = context.querySelectorAll(
          '.block-button-modal-block-wrapper',
        );
      }
      wrappers.forEach(function (wrapper) {
        const button = wrapper.querySelectorAll('button, input[type="submit"]');
        const block = wrapper.querySelector('.block');
        const width = button[0].getAttribute('data-block-button-modal-width');
        const height = button[0].getAttribute('data-block-button-modal-height');
        const options = {
          title: button[0].getAttribute('data-block-button-modal-title'),
          width: width || '100%',
          height: height || undefined,
        };

        if (!button[0] || !block) {
          return;
        }

        button[0].dialog = Drupal.dialog(
          `#${block.getAttribute('id')}`,
          options,
        );

        button[0].addEventListener('click', function (e) {
          e.preventDefault();
          this.dialog.showModal();
        });
      });
    },
  };
})(Drupal);
