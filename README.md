# Block Button Modal

The Block Button Modal module allows you to show a block's content in a modal
dialog.

The block is rendered in the page, but a button is visible instead, which is
used to open the modal dialog which contains the block.

For a full description of the module, visit the
[project page](https://www.drupal.org/project/block_button_modal).

Submit bug reports and feature suggestions, or track changes in the
[issue queue](https://www.drupal.org/project/issues/block_button_modal).
